import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: 'home_group',
        title: 'Home Group',
        translate: 'NAV.HOME',
        type: 'group',
        permission: 'PAGES_HOME',
        children: [
            {
                id: 'home',
                title: 'Home',
                translate: 'NAV.HOME',
                type: 'item',
                icon: 'home',
                url: '/pages/home',
                permission: 'PAGES_HOME',
            },
        ]
    },
];
