export const locale = {
    lang: 'es',
    data: {
        NAV: {
            HOME: 'Inicio',
            MANAGEMENT: {
                TITLE: 'Administración',
                COMPUTERS: {
                    TITLE: 'Computadoras'
                },
                SECTOR_TYPES: {
                    TITLE: 'Tipos de Sectores'
                },
            },
            SECURITY: {
                TITLE: 'Seguridad',
                ROLES_AND_PERMISSIONS: {
                    TITLE: 'Roles y Permisos'
                },
            }
        },
        COMMON: {
            LOADING: 'Cargando...',
            SEARCHING: 'Buscando',
            SEARCH: 'Buscar',
            ACTIONS: {
                MANAGE: 'Administrar'
            },
            NO_DATA_AVAILABLE: 'No hay datos disponibles',
            NOT_AVAILABLE: 'No disponible',
            BUTTONS: {
                ADD: 'Agregar',
                EDIT: 'Editar',
                DELETE: 'Borrar',
                SAVE: 'Guardar',
                SAVE_CHANGES: 'Guardar cambios',
                CANCEL: 'Cancelar',
                CLOSE: 'Cerrar',
                OK: 'Aceptar',
                ACCEPT: 'Aceptar',
            },
            DEFAULT_COLUMNS: {
                ID: 'Id',
                REGISTER_DATE: 'Fecha de registro',
                REGISTER_BY: 'Registrado por',
                UPDATED_DATE: 'Fecha de actualización',
                UPDATED_BY: 'Actualizado por',
                DELETED_DATE: 'Fecha de borrado',
                DELETED_BY: 'Borrado por',
                IS_DELETED: 'Borrado',
            },
            FIELDS: {
                NAME: 'Nombre',
                DESCRIPTION: 'Descripción'
            },
            SAVING: 'Guardando',
            SAVING_CHANGES: 'Guardando cambios',
            WARNINGS: {
                ATTENTION: 'Atención',
            },
            QUESTIONS: {
                DELETE_CONFIRMATION: '¿Esta seguro que quiere borrar "{{value}}"?'
            },
            MESSAGES: {
                REQUEST_PROCESSED_SUCCESSFULLY: 'Solicitud procesada correctamente.',
            },
        }
    }
};
