export const locale = {
    lang: 'en',
    data: {
        NAV: {
            HOME: 'Home',
            MANAGEMENT: {
                TITLE: 'Management',
                COMPUTERS: {
                    TITLE: 'Computers'
                },
                SECTOR_TYPES: {
                    TITLE: 'Sector Types'
                },
            },
            SECURITY: {
                TITLE: 'Security',
                ROLES_AND_PERMISSIONS: {
                    TITLE: 'Roles and Permissions'
                },
            }
        },
        COMMON: {
            LOADING: 'Loading...',
            SEARCHING: 'Searching',
            SEARCH: 'Search',
            ACTIONS: {
                MANAGE: 'Manage'
            },
            NO_DATA_AVAILABLE: 'No data available',
            NOT_AVAILABLE: 'Not available',
            BUTTONS: {
                ADD: 'Add',
                EDIT: 'Edit',
                DELETE: 'Delete',
                SAVE: 'Save',
                SAVE_CHANGES: 'Save changes',
                CANCEL: 'Cancel',
                CLOSE: 'Close',
                OK: 'Ok',
                ACCEPT: 'Accept',
            },
            DEFAULT_COLUMNS: {
                ID: 'Id',
                REGISTER_DATE: 'Register date',
                REGISTER_BY: 'Register by',
                UPDATED_DATE: 'Updated date',
                UPDATED_BY: 'Updated by',
                DELETED_DATE: 'Deleted date',
                DELETED_BY: 'Deleted by',
                IS_DELETED: 'Deleted',
            },
            FIELDS: {
                NAME: 'Name',
                DESCRIPTION: 'Description'
            },
            SAVING: 'Saving',
            SAVING_CHANGES: 'Saving changes',
            WARNINGS: {
                ATTENTION: 'Attention',
            },
            QUESTIONS: {
                DELETE_CONFIRMATION: 'Are you sure you want to delete "{{value}}"?'
            },
            MESSAGES: {
                REQUEST_PROCESSED_SUCCESSFULLY: 'Request processed successfully.',
            },
        }
    }
};
