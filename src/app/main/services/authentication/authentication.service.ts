import { Injectable } from '@angular/core';
import { Observable, Subscriber, BehaviorSubject } from 'rxjs';
import { NgxPermissionsService } from 'ngx-permissions';
import * as jwt_decode from "jwt-decode";
import { environment } from 'environments/environment';
import { Role } from 'app/main/models/roles/role';
import { Permission } from 'app/main/models/permissions/permission';
import { User } from 'app/main/models/users/user';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthenticationService {

    private userBehaviorSubject: BehaviorSubject<User>;
    public userObservable: Observable<User>;

    constructor(
        private http: HttpClient,
        private ngxPermissionsService: NgxPermissionsService
    ) {
        this.userBehaviorSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem(environment.localStorageAuthDataItem)));
        this.userObservable = this.userBehaviorSubject.asObservable();
    }

    public get user(): User {
        return this.userBehaviorSubject.value;
    }

    public setUser(user: User): void {
        this.userBehaviorSubject.next(user);
    }

    public login(username: string, password: string): Observable<boolean> {


        const params = new URLSearchParams();
        params.append('grant_type', 'password');
        params.append('username', username);
        params.append('password', password);
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append('Authorization', 'Basic YXV0aHNlcnZlcjp6M29LV2dwWDRKWnVsVmxnbkNDWDdmZkRrTmJRSDR6Vg==');
        console.log('EnviromentActual>>>>>', environment.api.auth);
        return this.http.post(environment.api.auth + 'auth/token', params.toString()).pipe(map((response: any) => {
            console.log('AuthenticationService > login > r', response);
            if (!response || !response.access_token) {
                return false;
            }
            const decodedToken = jwt_decode(response.access_token);
            console.log('AuthenticationService > login > r > decode token', decodedToken);
            const role1 = JSON.parse(decodedToken.Role);
            const role = new Role();
            role.Id = role1.Id;
            role.Name = role1.Name;
            role.Permissions = role1.Permissions;


            localStorage.setItem(environment.localStorageAuthDataItem, JSON.stringify({
                Token: response.access_token,
                UserCode: decodedToken.Id,
                FirstName: decodedToken.FirstName,
                LastName: decodedToken.LastName,
                Email: decodedToken.Mail,
                Photo: decodedToken.Photo,
                Language: decodedToken.Language ? decodedToken.Language : 'es',
                PhoneNumber: decodedToken.Telefono1,
                Role: role,
                UpdatedDate: decodedToken.UpdatedDate ? new Date(decodedToken.UpdatedDate) : null
            }));

            const user = new User();
            user.UserCode = decodedToken.Id;
            user.UpdatedDate = decodedToken.UpdatedDate ? new Date(decodedToken.UpdatedDate) : null;
            user.FirstName = decodedToken.FirstName;
            user.LastName = decodedToken.LastName;
            user.Email = decodedToken.Mail;
            user.Photo = decodedToken.Photo;
            user.Language = decodedToken.Language ? decodedToken.Language : 'es';
            user.Role = role;
            user.PhoneNumber = decodedToken.Telefono1;
            user.Token = response.access_token;
            this.setUser(user);
            console.log('AuthenticationService > login > this.user', this.user);
            return true;
        }));
    }

    public getPermissions(): Observable<string[]> {
        let authData = this.getAuthData();
        if (authData && authData.Role && authData.Role.Permissions && authData.Role.Permissions.length > 0) {
            let permissions: string[] = [];
            for (const permission of authData.Role.Permissions) {
                if (!permissions.find(p => p == permission.Id)) {
                    permissions.push(permission.Id);
                }
            }
            return Observable.create((observer: Subscriber<string[]>) => {
                observer.next(permissions);
                observer.complete();
            });
        }

        const token = this.getToken();
        const roleId = this.getRoleId();
        if (!token || !roleId) {
            return Observable.create((observer: Subscriber<string[]>) => {
                observer.next([]);
                observer.complete();
            });
        }

        // TODO Implementar el método /api/permissions/{roleId} en tu API, para obtener los permisos
        const headers = new HttpHeaders();
        headers.append('Authorization', 'Bearer ' + token);
        const options = {
            headers: headers
        };
        return this.http.get<Permission[]>(environment.api.base + 'Permissions/GetByRoleId/' + roleId, options).pipe(map(response => {
            console.log('AuthenticationService > getPermissions > response', response);
            const permissions: Permission[] = (<Permission[]>response).filter(p => p.IsGranted);
            authData.Role.Permissions = permissions;
            this.setAuthData(authData);
            const result: string[] = [];
            for (const permission of permissions) {
                if (!result.find(p => p == permission.Id)) {
                    result.push(permission.Id);
                }
                result.push(permission.Id);
            }
            return result;
        }, error => {
            console.error('AuthenticationService > getPermissions > error', error);
            return [];
        }));
    }

    public getAuthData(): any {
        return JSON.parse(localStorage.getItem(environment.localStorageAuthDataItem));
    }

    public setAuthData(authData: any) {
        localStorage.setItem(environment.localStorageAuthDataItem, JSON.stringify(authData));
    }

    public getToken(): string {
        let authData = this.getAuthData();
        if (authData && authData.Token) {
            return authData.Token;
        }
        return null;
    }

    public getLanguage(): string {
        let authData = this.getAuthData();
        if (authData) {
            return authData.Language;
        }
        return null;
    }

    public getUserCode(): string {
        let authData = this.getAuthData();
        if (authData) {
            return authData.UserCode;
        }
        return null;
    }

    public getRole(): Role {
        let authData = this.getAuthData();
        if (authData && authData.Role) {
            return authData.Role;
        }
        return null;
    }

    public getRoleId(): string {
        let authData = this.getAuthData();
        if (authData && authData.Role) {
            return authData.Role.Id;
        }
        return null;
    }

    public getRoleName(): string {
        let authData = this.getAuthData();
        if (authData && authData.Role) {
            return authData.Role.Name;
        }
        return null;
    }

    public logout(): void {
        this.ngxPermissionsService.flushPermissions();
        localStorage.removeItem(environment.localStorageAuthDataItem);
        this.userBehaviorSubject.next(null);
    }

}
