import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Permission } from 'app/main/models/permissions/permission';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BaseModel } from '../models/base.model';

@Injectable()
export class BaseService<T extends BaseModel> {

    public readonly HttpClient: HttpClient;
    private readonly Controller: string;
    private entitiesBehaviorSubject: BehaviorSubject<T[]>;
    public entitiesObservable: Observable<T[]>;
    private entityBehaviorSubject: BehaviorSubject<T>;
    public entityObservable: Observable<T>;

    constructor(
        private _httpClient: HttpClient,
        controller: string
    ) {
        this.HttpClient = _httpClient;
        this.Controller = controller;
        this.entitiesBehaviorSubject = new BehaviorSubject<T[]>([]);
        this.entitiesObservable = this.entitiesBehaviorSubject.asObservable();
        this.entityBehaviorSubject = new BehaviorSubject<T>(null);
        this.entityObservable = this.entityBehaviorSubject.asObservable();
    }

    public get entities(): T[] {
        return this.entitiesBehaviorSubject.value;
    }

    public setEntities(entities: T[]): void {
        this.entitiesBehaviorSubject.next(entities);
    }

    public get entity(): T {
        return this.entityBehaviorSubject.value;
    }

    public setEntity(entity: T): void {
        this.entityBehaviorSubject.next(entity);
    }

    public getAll(): Observable<T[]> {
        const url: string = `${this.Controller}/all`;
        return this.HttpClient.get<T[]>(url).pipe(map(response => {
            this.setEntities(response);
            return response;
        }));
    }

    public getById(id: string): Observable<T> {
        const url: string = `${this.Controller}/id/${id}`;
        return this.HttpClient.get<T>(url).pipe(map(response => {
            this.setEntity(response);
            return response;
        }));
    }

    public count(): Observable<number> {
        const url: string = `${this.Controller}/count`;
        return this.HttpClient.get<number>(url).pipe(map(response => {
            return response;
        }));
    }

    public add(entity: T) {
        const url: string = `${this.Controller}/add`;
        return this.HttpClient.post(url, entity).pipe(map(response => {
            this.entities.push(entity);
            this.setEntities(this.entities);
        }));
    }

    public edit(id: string, entity: T) {
        const url: string = `${this.Controller}/edit/${id}`;
        return this.HttpClient.put(url, entity).pipe(map(response => {
            const index = this.entities.findIndex(e => e.Id == id);
            if (index >= 0) {
                this.entities[index] = entity;
                this.setEntities(this.entities);
            }
        }));
    }

    public delete(id: string) {
        const url: string = `${this.Controller}/delete/${id}`;
        return this.HttpClient.delete(url).pipe(map(response => {
            const index = this.entities.findIndex(e => e.Id == id);
            if (index >= 0) {
                this.entities.splice(index, 1);
                this.setEntities(this.entities);
            }
        }));
    }
}
