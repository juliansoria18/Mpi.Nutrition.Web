import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Role } from 'app/main/models/roles/role';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class UniversalService {

    constructor(
        private http: HttpClient
    ) { }

    // TODO Implementar el método api/universal/GetRoles
    public getRoles(): Observable<Role[]> {
        const url: string = 'universal/GetRoles';
        return this.http.get<Role[]>(url).pipe(map(response => response));
    }
}
