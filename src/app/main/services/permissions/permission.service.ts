import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Permission } from 'app/main/models/permissions/permission';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class PermissionService {

    constructor(
        private http: HttpClient
    ) { }

    // TODO Implementar el método api/permissions/all en tu API
    public getAll(): Observable<Permission[]> {
        const url: string = 'permissions/all';
        return this.http.get<Permission[]>(url).pipe(map(response => response));
    }

    // TODO Implementar el método api/permissions/GetByRoleId/{roleId} e tu API
    public getByRoleId(roleId: string): Observable<Permission[]> {
        const url: string = 'permissions/GetByRoleId/' + roleId;
        return this.http.get<Permission[]>(url).pipe(map(response => response));
    }
}
