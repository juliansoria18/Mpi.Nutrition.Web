import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { Error400Component } from 'app/main/pages/errors/400/error-400.component';
import { TranslateModule } from '@ngx-translate/core';

const routes = [
    {
        path: 'errors/error-400',
        component: Error400Component
    }
];

@NgModule({
    declarations: [
        Error400Component
    ],
    imports: [
        RouterModule.forChild(routes),

        TranslateModule,

        MatIconModule,

        FuseSharedModule
    ]
})
export class Error400Module {
}
