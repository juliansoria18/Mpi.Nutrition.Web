import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { Error403Component } from 'app/main/pages/errors/403/error-403.component';
import { TranslateModule } from '@ngx-translate/core';

const routes = [
    {
        path: 'errors/error-403',
        component: Error403Component
    }
];

@NgModule({
    declarations: [
        Error403Component
    ],
    imports: [
        RouterModule.forChild(routes),

        TranslateModule,

        MatIconModule,

        FuseSharedModule
    ]
})
export class Error403Module {
}
