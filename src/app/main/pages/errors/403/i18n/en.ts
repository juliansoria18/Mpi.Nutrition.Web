export const locale = {
    lang: 'en',
    data: {
        PAGES: {
            ERRORS: {
                403: {
                    DETAIL: 'You do not have sufficient permissions to perform this action.',
                    GO_BACK_BUTTON: 'Go back to home'
                }
            }
        }
    }
};
