import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { Error401Component } from 'app/main/pages/errors/401/error-401.component';
import { TranslateModule } from '@ngx-translate/core';

const routes = [
    {
        path: 'errors/error-401',
        component: Error401Component
    }
];

@NgModule({
    declarations: [
        Error401Component
    ],
    imports: [
        RouterModule.forChild(routes),

        TranslateModule,

        MatIconModule,

        FuseSharedModule
    ]
})
export class Error401Module {
}
