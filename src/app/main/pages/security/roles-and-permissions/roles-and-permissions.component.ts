import { Component, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { Role } from 'app/main/models/roles/role';
import { Permission } from 'app/main/models/permissions/permission';
import { PermissionService } from 'app/main/services/permissions/permission.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { combineLatest } from 'rxjs/internal/observable/combineLatest';
import { takeUntil } from 'rxjs/operators';
import { UniversalService } from 'app/main/services/universal/universal.service';

import { locale as english } from './i18n/en';
import { locale as spanish } from './i18n/es';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { SweetAlert2Helper } from 'app/helpers/sweet-alert-2.helper';

@Component({
    moduleId: module.id,
    selector: 'roles-and-permissions',
    templateUrl: 'roles-and-permissions.component.html',
    styleUrls: ['roles-and-permissions.component.scss']
})
export class RolesAndPermissionsComponent implements OnInit, OnChanges, OnDestroy {
    
    @BlockUI('bw-blockui') blockUI: NgBlockUI;
    private _unsubscribeAll: Subject<any>;
    roles: Role[];
    permissions: Permission[];

    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _sweetAlert2Helper: SweetAlert2Helper,
        private _universalService: UniversalService,
        private _permissionService: PermissionService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(english, spanish);
        this._unsubscribeAll = new Subject();
        this.roles = [];
        this.permissions = [];
    }

    ngOnInit() {
        this._fuseTranslationLoaderService.loadTranslations(english, spanish);
        this.blockUI.start('Cargando...');
        combineLatest(
            this._universalService.getRoles(),
            this._permissionService.getAll(),
        ).pipe(takeUntil(this._unsubscribeAll))
            .subscribe(
                ([roles, permissions]) => {
                    this.roles = roles;
                    for (const role of this.roles) {
                        role.Permissions = [];
                        role.IsAllPermissionsChecked = false;
                    }
                    this.permissions = permissions;
                    console.log('RolesAndPermissionsComponent > ngOnInit > this.roles', this.roles);
                    console.log('RolesAndPermissionsComponent > ngOnInit > this.permissions', this.permissions);
                    this.blockUI.stop();
                }, error => {
                    console.log('RolesAndPermissionsComponent > ngOnInit > Error', error);
                    this._sweetAlert2Helper.error('Error', 'Ocurrió un error recuperando roles y permisos. Detalle: ' + error.Message, null, true);
                    this.blockUI.stop();
                });
    }

    ngOnChanges() {

    }

    ngOnDestroy() {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    openAddRoleDialog() {

    }

    getPermissionsByRoleId(roleId: string) {
        let role = this.roles.find(r => r.Id == roleId);
        if (role.Permissions.length > 0) {
            return;
        }
        this._permissionService.getByRoleId(roleId).subscribe(response => {
            role.Permissions = response;
            this.checkIfIsAllPermissionsChecked(role);
        }, error => {
            this._sweetAlert2Helper.error('Error', 'Ocurrió un error recuperando permisos. Detalle: ' + error.Message, null, true);
        });
    }

    checkIfIsAllPermissionsChecked(role: Role, permission?: Permission) {
        if (permission) {
            let permission2 = role.Permissions.find(p => p.Id == permission.Id);
            permission2.IsGranted = !permission2.IsGranted;
        }
        // console.log('RolesAndPermissionsComponent > checkIfIsAllPermissionsChecked > role', role);
        let isAllPermissionsChecked = true;
        for (const permission of role.Permissions) {
            if (!permission.IsGranted) {
                isAllPermissionsChecked = false;
                break;
            }
        }
        role.IsAllPermissionsChecked = isAllPermissionsChecked;
    }

    checkToggle(role: Role) {
        // console.log('RolesAndPermissionsComponent > checkToggle > role', role);
        role.IsAllPermissionsChecked = !role.IsAllPermissionsChecked;
        for (const permission of role.Permissions) {
            if (permission.Id != 'PAGES_HOME') {
                permission.IsGranted = role.IsAllPermissionsChecked;
            }
        }
    }

    saveChanges(role: Role) {

    }

    removeRole(role: Role, $event: any) {
        $event.preventDefault();
        $event.stopPropagation();
    }
}
