export const locale = {
    lang: 'es',
    data: {
        PAGES: {
            ROLES_AND_PERMISSIONS: {
                TITLE: 'Roles y Permisos',
                DESCRIPTION: '',
                ACTIONS: {
                    ADD_ROLE: 'Agregar rol',
                    EDIT_ROLE: 'Editar rol',
                    DELETE_ROLE: 'Borrar rol',
                    SAVE_CHANGES: 'Guardar cambios',
                    CHECK_ALL: 'Marcar todo',
                    UNCHECK_ALL: 'Desmarcar todo'
                }
            }
        }
    }
};
