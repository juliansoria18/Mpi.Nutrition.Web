export const locale = {
    lang: 'en',
    data: {
        PAGES: {
            ROLES_AND_PERMISSIONS: {
                TITLE: 'Roles and Permissions',
                DESCRIPTION: '',
                ACTIONS: {
                    ADD_ROLE: 'Add role',
                    EDIT_ROLE: 'Edit role',
                    DELETE_ROLE: 'Delete role',
                    SAVE_CHANGES: 'Save changes',
                    CHECK_ALL: 'Check all',
                    UNCHECK_ALL: 'Uncheck all'
                }
            }
        }
    }
};
