// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { RolesAndPermissionsComponent } from './roles-and-permissions.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatIconModule, MatButtonModule, MatExpansionModule, MatCheckboxModule, MatTooltipModule } from '@angular/material';
import { UniversalService } from 'app/main/services/universal/universal.service';
import { PermissionService } from 'app/main/services/permissions/permission.service';

const routes = [
    {
        path: 'roles-and-permissions',
        component: RolesAndPermissionsComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
            permissions: {
                only: 'PAGES_SECURITY_ROLES_AND_PERMISSIONS',
                redirectTo: '/pages/errors/error-403'
            }
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule,

        MatIconModule,
        MatButtonModule,
        MatExpansionModule,
        MatCheckboxModule,
        MatTooltipModule,
    ],
    declarations: [
        RolesAndPermissionsComponent,
    ],
    exports: [
        RolesAndPermissionsComponent,
    ],
    providers: [
        UniversalService,
        PermissionService
    ]
})
export class RolesAndPermissionsModule {

}
