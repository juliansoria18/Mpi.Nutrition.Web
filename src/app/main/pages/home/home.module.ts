import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { HomeComponent } from './home.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { MatButtonModule } from '@angular/material';

const routes = [
    {
        path: 'home',
        component: HomeComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
            permissions: {
                only: '',
                redirectTo: '/pages/errors/error-403'
            }
        }
    }
];

@NgModule({
    declarations: [
        HomeComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        TranslateModule,
        FuseSharedModule,

        MatButtonModule,
    ],
    exports: [
        HomeComponent
    ],
    providers: [
    ]
})

export class HomeModule {
}
