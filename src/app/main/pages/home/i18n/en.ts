export const locale = {
    lang: 'en',
    data: {
        PAGES: {
            HOME: {
                TITLE: 'MPI.Nutrición.Web',
                DESCRIPTION: 'Welcome to the Base web application.',
                COMPUTERS: 'Computers',
                COMPONENTS: {
                    COMPONENT: 'Component',
                    COMPONENTS: 'Components',
                    HARD_DISK: 'Hard Disk',
                    HARD_DISKS: 'Hard Disks',
                    KEYBOARD: 'Keyboard',
                    KEYBOARDs: 'Keyboards',
                    MONITOR: 'Monitor',
                    MONITORS: 'Monitors',
                    MOTHERBOARD: 'Motherboard',
                    MOTHERBOARDS: 'Motherboards',
                    MOUSE: 'Mouse',
                    MOUSES: 'Mouses',
                    PROCESSOR: 'Processor',
                    PROCESSORS: 'Processors',
                },
                SECTORS: 'Sectors',
            }
        }
    }
};
