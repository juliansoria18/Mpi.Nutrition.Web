import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as spanish } from './i18n/es';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
    @BlockUI('bw-blockui') blockUI: NgBlockUI;
    @BlockUI('bw-blockui-computers') computersBlockUI: NgBlockUI;
    @BlockUI('bw-blockui-components') componentsBlockUI: NgBlockUI;
    @BlockUI('bw-blockui-sectors') sectorsBlockUI: NgBlockUI;

    maxHeight: number;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(english, spanish);
        this.maxHeight = 0;
    }

    ngOnInit() {
        
    }
}
