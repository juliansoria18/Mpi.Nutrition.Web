import { NgModule } from '@angular/core';
import { LoginModule } from './authentication/login/login.module';
import { HomeModule } from './home/home.module';
import { Error400Module } from './errors/400/error-400.module';
import { Error401Module } from './errors/401/error-401.module';
import { Error403Module } from './errors/403/error-403.module';
import { Error404Module } from './errors/404/error-404.module';
import { Error500Module } from './errors/500/error-500.module';
import { RolesAndPermissionsModule } from './security/roles-and-permissions/roles-and-permissions.module';

@NgModule({
    imports: [
        // Authentication
        LoginModule,

        // Errors
        Error400Module,
        Error401Module,
        Error403Module,
        Error404Module,
        Error500Module,

        // Home
        HomeModule,
        
        // Roles and Permissions
        RolesAndPermissionsModule,
    ]
})
export class PagesModule
{

}
