import { Role } from '../roles/role';

export class User {
    UserCode: string;
    UpdatedDate: Date;
    FirstName: string;
    LastName: string;
    Email: string;
    Photo: string;
    Role: Role;
    Language: string;
    PhoneNumber: string;
    Token: string;

    constructor() {
        this.UserCode = '';
        this.UpdatedDate = null;
        this.FirstName = '';
        this.LastName = '';
        this.Email = '';
        this.Photo = '';
        this.Role = null;
        this.Language = '';
        this.PhoneNumber = '';
        this.Token = '';
    }
}