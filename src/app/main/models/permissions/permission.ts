import { BaseModel } from './../base.model';

export class Permission extends BaseModel {
    Name: string;
    Description: string;
    Ordering: number;
    IsGranted: boolean;

    constructor() {
        super();
        this.Name = '';
        this.Description = '';
        this.Ordering = 0;
        this.IsGranted = false;
    }
}