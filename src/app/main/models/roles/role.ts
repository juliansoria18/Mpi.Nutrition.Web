import { BaseModel } from './../base.model';
import { Permission } from "../permissions/permission";

export class Role extends BaseModel {
    Name: string;
    Permissions: Permission[];
    IsAllPermissionsChecked: boolean;

    constructor() {
        super();
        this.Name = '';
        this.Permissions = [];
        this.IsAllPermissionsChecked = false;
    }
}