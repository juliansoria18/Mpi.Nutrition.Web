import { ValidatorFn } from '@angular/forms';

export class BaseTableColumn {
    Index: number;
    Name?: string;
    Type?: Type;
    TranslationCode?: string;
    FlexGrow?: number;
    Sortable?: boolean;
    Validators?: ValidatorFn[];

    constructor() {
        this.Index = 0;
        this.Name = '';
        this.Type = Type.SINGLE_LINE_STRING;
        this.TranslationCode = '';
        this.FlexGrow = 1;
        this.Sortable = true;
        this.Validators = []
    }
}

export enum Type {
    SINGLE_LINE_STRING,
    MULTIPLE_LINE_STRING,
    NUMBER,
    DATE,
    BOOLEAN,
    ARRAY,
}