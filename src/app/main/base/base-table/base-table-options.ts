export class BaseTableOptions {
    Limit: number;
    DefaultColumns: any;
    DefaultActions: any;

    constructor() {
        this.Limit = 10;
        this.DefaultColumns = {
            Id: {
                Show: false,
                FlexGrow: 1,
                Sortable: false,
            },
            RegisterDate: {
                Show: false,
                FlexGrow: 1,
                Sortable: true,
            },
            RegisterBy: {
                Show: false,
                FlexGrow: 1,
                Sortable: true,
            },
            UpdatedDate: {
                Show: false,
                FlexGrow: 1,
                Sortable: true,
            },
            UpdatedBy: {
                Show: false,
                FlexGrow: 1,
                Sortable: true,
            },
            DeletedDate: {
                Show: false,
                FlexGrow: 1,
                Sortable: true,
            },
            DeletedBy: {
                Show: false,
                FlexGrow: 1,
                Sortable: true,
            },
            IsDeleted: {
                Show: false,
                FlexGrow: 1,
                Sortable: false,
            },
        };
        this.DefaultActions = {
            FlexGrow: 1,
            Add: {
                Show: true,
                RequestPermission: true,
                Permission: 'YOUR_PERMISSION',
            },
            Edit: {
                Show: true,
                RequestPermission: true,
                Permission: 'YOUR_PERMISSION',
            },
            Delete: {
                Show: true,
                RequestPermission: true,
                Permission: 'YOUR_PERMISSION',
            },
        };
    }
}