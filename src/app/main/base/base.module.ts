// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { BaseComponent } from './base.component';
import { BaseTableComponent } from './base-table/base-table.component';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MatButtonModule, MatMenuModule, MatIconModule, MatFormFieldModule, MatInputModule, MatTooltipModule, MatToolbarModule } from '@angular/material';
import { DatePipe } from '@angular/common';
import { BaseDialogComponent } from './base-dialog/base-dialog.component';

@NgModule({
    imports: [
        TranslateModule,
        FuseSharedModule,

        NgxDatatableModule,
        MatButtonModule,
        MatMenuModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatTooltipModule,
        MatToolbarModule,
    ],
    declarations: [
        BaseComponent,
        BaseTableComponent,
        BaseDialogComponent,
    ],
    exports: [
        BaseComponent,
        BaseTableComponent,
        BaseDialogComponent,
    ],
    entryComponents: [
        BaseDialogComponent,
    ],
    providers: [
        DatePipe,
    ]
})
export class BaseModule {

}
