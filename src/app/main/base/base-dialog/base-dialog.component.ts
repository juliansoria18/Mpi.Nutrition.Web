import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { isEqual, omit } from 'lodash';
import { SweetAlert2Helper } from 'app/helpers/sweet-alert-2.helper';
import { BaseTableColumn, Type } from '../base-table/base-table-column';
import { KeyValue } from '@angular/common';

export interface DialogData {
    titleTranslationCode: string;
    action: string;
    columns: {[key: string]: BaseTableColumn};
    row: any;
}

@Component({
    selector: 'base-dialog',
    templateUrl: 'base-dialog.component.html',
    styleUrls: ['base-dialog.component.scss']
})
export class BaseDialogComponent {

    dialogForm: FormGroup;
    title: string;
    action: string;
    columns: {[key: string]: BaseTableColumn};
    row: any;
    rowCopy: any;
    type = Type;
    saveCallback: (any) => void;

    constructor(
        public _matDialogRef: MatDialogRef<BaseDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _dialogData: DialogData,
        private _formBuilder: FormBuilder,
        private _sweetAlert2Helper: SweetAlert2Helper,
    ) {
        this.title = _dialogData.titleTranslationCode ? _dialogData.titleTranslationCode : 'COMMON.NOT_AVAILABLE';
        this.action = _dialogData.action ? _dialogData.action : 'add';
        this.columns = _dialogData.columns;
        this.row = JSON.parse(JSON.stringify(_dialogData.row));
        this.rowCopy = JSON.parse(JSON.stringify(this.row));
        console.log('BaseDialogComponent > constructor > this._data', this._dialogData);
        console.log('BaseDialogComponent > constructor > this.columns', this.columns);
        console.log('BaseDialogComponent > constructor > this.row', this.row);
        this.dialogForm = this.createDialogForm();
        for (const key in this.columns) {
            if (this.row.hasOwnProperty(key)) {
                const validators = this.columns[key].Validators ? this.columns[key].Validators : [];
                this.addFormGroupControl(key, validators, this.row[key]);
            }
        }
        this.saveCallback = null;
    }

    createDialogForm(): FormGroup {
        const formGroup = this._formBuilder.group({
            Id: [this.row.Id, [Validators.required]],
            RegisterDate: [this.row.RegisterDate, [Validators.required]],
            RegisterBy: [this.row.RegisterBy, [Validators.required, Validators.maxLength(128)]],
            UpdatedDate: [this.row.UpdatedDate, [Validators.required]],
            UpdatedBy: [this.row.UpdatedBy, [Validators.required, Validators.maxLength(128)]]
        });        
        return formGroup;
    }

    sortByIndexAscending = (kv1: KeyValue<string, any>, kv2: KeyValue<string, any>): number => {
        const a = kv1.value.Index;
        const b = kv2.value.Index;
        return a > b ? 1 : (b > a ? -1 : 0);
    };

    addFormGroupControl(formGroupControlName: string, validators: ValidatorFn[], value?: any) {
        if (!this.dialogForm) return;
        this.dialogForm.addControl(formGroupControlName, new FormControl(value ? value : '', validators));
    }

    removeFormGroupControl(formGroupControlName: string) {
        if (!this.dialogForm) return;
        this.dialogForm.removeControl(formGroupControlName);
    }

    setRawValues() {
        const rawValue = this.dialogForm.getRawValue();
        for (const key in this.columns) {
            if (this.row.hasOwnProperty(key)) {
                this.row[key] = rawValue[key];
            }
        }
    }

    cancel() {
        this.setRawValues();
        console.log('BaseDialogComponent > cancel > this.row', this.row);
        console.log('BaseDialogComponent > cancel > this.rowCopy', this.rowCopy);
        const hasChanges = !isEqual(omit(this.row, ['__proto__']), omit(this.rowCopy, ['__proto__']));
        if (!hasChanges) {
            this._matDialogRef.close();
            return;
        }
        this._sweetAlert2Helper.question('Atención', '¿Descartar cambios?', 'Descartar', 'Cancelar', () => {
            this._matDialogRef.close();
        }, () => {
        });
    }

    save() {
        this.setRawValues();
        this.row.RegisterDate = this.action === 'edit' ? this.row.RegisterDate : new Date();
        this.row.UpdatedDate = new Date();
        if (this.saveCallback) {
            this.saveCallback(this.row);
        }
    }
}
