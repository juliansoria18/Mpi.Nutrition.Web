import { TranslateService, LangChangeEvent, TranslationChangeEvent } from '@ngx-translate/core';
import { MatPaginatorIntl } from "@angular/material";
import { Inject } from '@angular/core';
import { LocatorService } from '../../locator.service';

export class CustomMatPaginatorIntl extends MatPaginatorIntl {
    itemsPerPageLabel = 'Records per page';
    nextPageLabel = 'Next';
    previousPageLabel = 'Previous';
    ofText = 'of';

    translateService: any;

    constructor(
    ) {
        super();
        this.translateService = LocatorService.injector.get(TranslateService);
        this.translateService.get(['FIELDS.RECORDS_PER_PAGE', 'FIELDS.OF', 'FIELDS.PREVIOUS', 'FIELDS.NEXT']).subscribe(res => {
            console.log('TRANSLATE', res);
            this.itemsPerPageLabel = res['FIELDS.RECORDS_PER_PAGE'];
            this.nextPageLabel = res['FIELDS.NEXT'];
            this.previousPageLabel = res['FIELDS.PREVIOUS'];
            this.ofText = res['FIELDS.OF'];
        });
        // this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
        //     console.log('onLangChange');
        //     this.translateService.get(['FIELDS.RECORDS_PER_PAGE', 'FIELDS.OF', 'FIELDS.PREVIOUS', 'FIELDS.NEXT']).subscribe(res => {
        //         console.log('TRANSLATE', res);
        //         this.itemsPerPageLabel = res['FIELDS.RECORDS_PER_PAGE'];
        //         this.nextPageLabel = res['FIELDS.NEXT'];
        //         this.previousPageLabel = res['FIELDS.PREVIOUS'];
        //         this.ofText = res['FIELDS.OF'];
        //     });
        // });
    }

    getRangeLabel = function (page, pageSize, length) {
        if (length === 0 || pageSize === 0) {
            return `0 ${this.ofText} ${length}`;
        }
        length = Math.max(length, 0);
        const startIndex = page * pageSize;
        // If the start index exceeds the list length, do not try and fix the end index to the end.
        const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
        return `${startIndex + 1} - ${endIndex} ${this.ofText} ${length}`;
    };

    // getRangeLabel: (page: number, pageSize: number, length: number) => {
    //     if (length == 0 || pageSize == 0) {
    //         return `0 of ${length}`;
    //     } 
    //     length = Math.max(length, 0);
    //     const startIndex = page * pageSize;
    //     // If the start index exceeds the list length, do not try and fix the end index to the end.
    //     const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    //     return `${startIndex + 1} - ${endIndex} of ${length}`;
    // }
}