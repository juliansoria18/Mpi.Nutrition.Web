import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';

import { FuseDirectivesModule } from '@fuse/directives/directives';
import { FusePipesModule } from '@fuse/pipes/pipes.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { TranslateModule } from '@ngx-translate/core';
import { BlockUIModule } from 'ng-block-ui';
import { TranslationService } from 'app/main/services/translation/translation.service';
import { MatDialogModule, MatSnackBarModule } from '@angular/material';
import { SweetAlert2Helper } from 'app/helpers/sweet-alert-2.helper';
import { MatSnackBarHelper } from 'app/helpers/mat-snack-bar.helper';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        FlexLayoutModule,

        FuseDirectivesModule,
        FusePipesModule,
        TranslateModule,
        MatDialogModule, // FIX Esto corrige el siguiente error: No component factory found for BaseDialogComponent. Did you add it to @NgModule.entryComponents?
        MatSnackBarModule,
    ],
    exports: [
        NgxPermissionsModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        FlexLayoutModule,

        FuseDirectivesModule,
        FusePipesModule,
        TranslateModule,
        MatDialogModule, // FIX Esto corrige el siguiente error: No component factory found for BaseDialogComponent. Did you add it to @NgModule.entryComponents?
        MatSnackBarModule,
        
        BlockUIModule,
    ],
    providers: [
        TranslationService,
        SweetAlert2Helper,
        MatSnackBarHelper,
    ]
})
export class FuseSharedModule {
}
