// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    hmr: false,
    localStorageAuthDataItem: 'MPINutritionWebAuthDataLocal',
    api: {
        
        // TODO Levantar API de autenticación en tu máquina local o en el servidor de desarrollo 233 (no calidad)
        auth: 'http://localhost:20346/api/', // API Autenticación
        base: 'http://localhost:20346/api/', // API Base
    },
    google: {
        maps: {
            apiKey: 'AIzaSyC2XSrOzRDT389SwU5bosu5UMzsXDJLRlw'
        }
    },
    appCode: 'COD_NUTRITION',
    appName: 'MPI.Nutrition.Web',
    appVersion: '1.0.0',
    appVersionDate: '25/06/2019'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
