export const environment = {
    production: true,
    hmr: false,
    localStorageAuthDataItem: 'MPIBaseWebAuthDataProduction',
    api: {
        auth: 'https://mpiauthapi.azurewebsites.net/', // API Autenticación
        base: 'http://localhost:5000/api/', // API Base
    },
    google: {
        maps: {
            apiKey: 'AIzaSyC2XSrOzRDT389SwU5bosu5UMzsXDJLRlw'
        }
    },
    appCode: 'COD_BASE',
    appName: 'MPI.Nutrición.Web',
    appVersion: '1.0.0',
    appVersionDate: '25/06/2019'
};
